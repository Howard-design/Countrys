import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { Provider } from 'react-redux';
import generateStore from './redux/store';

//#App.css
import './App.css';

//#Components
import Header from './components/Header';
import CountrysList from './components/CountrysList';
import CountryForName from './components/CountryForName';
import PageError from './components/PageError';

function App() {
  const store = generateStore();

  return (
    <Router>
      <Provider store={store}>
        <Switch>
          <Route path="/" exact>
            <Header />
            <CountrysList />
          </Route>
          <Route path="/:name" exact>
            <Header />
            <CountryForName />
          </Route>
          <Route component={PageError} />
        </Switch>
      </Provider>
    </Router>
  );
}

export default App;
