import axios from 'axios';

//#State
const initialState = {
    countrys: [],
    countrysName: [],
    status: '',
    statusName: ''
}

//#Const

//#List
const GET_COUNTRYS = 'GET_COUNTRYS';
const GET_COUNTRYS_SUCCESS = 'GET_COUNTRYS_SUCCESS';
const GET_COUNTRYS_ERROR = 'GET_COUNTRYS_ERROR';

//#View
const GET_NAME_COUNTRYS = 'GET_NAME_COUNTRYS';
const GET_NAME_COUNTRYS_SUCCESS = 'GET_NAME_COUNTRYS_SUCCESS';
const GET_NAME_COUNTRYS_ERROR = 'GET_NAME_COUNTRYS_ERROR';

//#Reducer
export default function countryReducer(state = initialState, action){
    switch (action.type) {
        case GET_COUNTRYS:
            return {
                ...state,
                status: action.payload
            }
        case GET_COUNTRYS_SUCCESS:
            return {
                ...state,
                countrys: action.payload,
                status: 200
            }
        case GET_COUNTRYS_ERROR:
            return {
                ...state,
                status: action.payload
            }
        case GET_NAME_COUNTRYS:
            return {
                ...state,
                statusName: action.payload
            }
        case GET_NAME_COUNTRYS_SUCCESS:
            return {
                ...state,
                countrysName: action.payload,
                statusName: 200
            }
        case GET_NAME_COUNTRYS_ERROR:
            return {
                ...state,
                statusName: action.payload
            }
    
        default:
            return state;
    }
}

//#Actions
export const getCountrys = () => async (dispatch, getState) => {
    dispatch({
        type: 'GET_COUNTRYS',
        payload: 'Loading...'
    });
    try {
        const res = await axios.get('https://restcountries.eu/rest/v2/all');
        res.status === 200 ? dispatch({
            type: 'GET_COUNTRYS_SUCCESS',
            payload: res.data
        }) : dispatch({
            type: 'GET_COUNTRYS_ERROR',
            payload: 404
        })
    } catch (error) {
        dispatch({
            type: 'GET_COUNTRYS_ERROR',
            payload: error
        });
    }
}

export const getNameCountrys = (name) => async (dispatch, getState) => {
    if (name.length >= 1) {
        dispatch({
            type: 'GET_NAME_COUNTRYS',
            payload: 'Loading...'
        });
        try {
            const res = await axios.get(`https://restcountries.eu/rest/v2/name/${name}`);
            res.status === 200 ? dispatch({
                type: 'GET_NAME_COUNTRYS_SUCCESS',
                payload: res.data
            }) : dispatch({
                type: 'GET_NAME_COUNTRYS_ERROR',
                payload: 404
            })
        } catch (error) {
            dispatch({
                type: 'GET_NAME_COUNTRYS_ERROR',
                payload: error
            });
        }
    }
}