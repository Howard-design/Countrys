import React from 'react';

//#Make Styles
import { makeStyles } from '@material-ui/core/styles';

//#Components
import Country from './Country';
import CountrySearch from './CountrySearch';

const useStyles = makeStyles(theme => ({
    bodyBackground: {
    background: 'whitesmoke',
    },
}));

function CountrysList() {
    const classes = useStyles();

    return (
        <div className={classes.bodyBackground}>
            <div className="container">
                <div className="row">
                    <div className="col-lg-6">
                        <CountrySearch />
                    </div>
                </div>
                <Country />
            </div>
        </div>
    )
}

export default CountrysList;
