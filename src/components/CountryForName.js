import React, { useEffect } from 'react';
import {
    Link
} from "react-router-dom";
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

//#Redux Actions
import { getNameCountrys } from '../redux/Countrys/countryDucks';

//#Make Styles
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    bodyBackground: {
        background: 'whitesmoke',
    },
    countryTitle: {
        fontWeight: 700,
    },
}));

function CountryForName() {
    const classes = useStyles();

    const { name } = useParams();

    const dispatch = useDispatch();

    const countrys = useSelector(store => store.countrys.countrysName);

    const statusName = useSelector(store => store.countrys.statusName);

    useEffect(() => {
        dispatch(getNameCountrys(name));
    }, [dispatch]);

    return (
        <div className={classes.bodyBackground}>
            <div className="container">
                <div className="pt-4 mb-5">
                    <Link to="/">
                        <button className="btn btn-info">Back</button>
                    </Link>
                </div>
                {
                    countrys.map(country => (
                        <div className="row rowHeight justify-content-center" key={country.name}>
                            <div className="col-lg-6">
                                <img src={country.flag} alt="" className="d-block w-100"/>
                            </div>
                            <div className="col-lg-6">
                                <div>
                                    <h4 className={classes.countryTitle}>{country.name}</h4>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-lg-6">
                                        <p><strong>Native Name:</strong> {country.nativeName}</p>
                                        <p><strong>Population:</strong> {country.population}</p>
                                        <p><strong>Region:</strong> {country.region}</p>
                                        <p><strong>Sub Region:</strong> {country.subregion}</p>
                                        <p><strong>Capital:</strong> {country.capital}</p>
                                    </div>
                                    <div className="col-lg-6">
                                        <p><strong>Top Level Domain:</strong> {country.topLevelDomain}</p>
                                        <p><strong>Currencies:</strong> {country.currencies[0].name}</p>
                                        <p><strong>Languages:</strong> {country.languages[0].name}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))
                }
                {
                    statusName === 'Loading...' ? (
                        <div className="row rowHeight justify-content-center mt-5 pt-5">
                            <div className="spinner-border text-primary" role="status">
                                <span className="sr-only">Loading...</span>
                            </div>
                        </div>
                    ) : ('')
                }
            </div>
        </div>
    )
}

export default CountryForName;
