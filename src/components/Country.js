import React, { useEffect } from 'react';
import {
    Link
} from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';

//#Redux Actions
import { getCountrys } from '../redux/Countrys/countryDucks';

function Country() {
    const dispatch = useDispatch();

    const countrys = useSelector(store => store.countrys.countrys);

    const status = useSelector(store => store.countrys.status);

    useEffect(() => {
        dispatch(getCountrys());
    }, [dispatch]);

    return (
        <React.Fragment>
            <div className="row justify-content-center">
                {
                    countrys.map(country => (
                        <div className="col-xl-3 col-lg-4 col-md-6 pl-lg-4 pr-lg-4 pl-5 pr-5" key={country.name}>
                            <Link to={`${country.name}`}>
                                <div className="card card-height mt-5">
                                    <div className="card-img-top">
                                        <img src={country.flag} alt="" width="100%" height="200" />
                                    </div>
                                    <div className="card-body">
                                        <div className="countryTitle">
                                            <h4>{country.name}</h4>
                                        </div>
                                        <div className="countryInfo mt-3">
                                            <p className="mt-3"><strong>Population:</strong> <span>{country.population}</span></p>
                                            <p className="mt-3"><strong>Region:</strong> <span>{country.region}</span></p>
                                            <p className="mt-3"><strong>Capital:</strong> <span>{country.capital}</span></p>
                                        </div>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    ))
                }
                {
                    status === 'Loading...' ? (
                        <div className="row rowHeight align-items-center">
                            <div className="spinner-border text-primary" role="status">
                                <span className="sr-only">Loading...</span>
                            </div>
                        </div>
                    ) : ('')
                }
                {
                    status === 404 ? (
                        <h6>Oops..! An error has occurred</h6>
                    ) : ('')
                }
            </div>
        </React.Fragment>
    )
}

export default Country;
