import React from 'react';

function PageError() {
    return (
        <div>
            <h3>404 Not Found</h3>
        </div>
    )
}

export default PageError;
