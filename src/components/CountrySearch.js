import React, { useState } from 'react';
import {
    BrowserRouter as Router,
    Link
} from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { getNameCountrys } from '../redux/Countrys/countryDucks';

function CountrySearch() {
    const dispatch = useDispatch();

    const countrysName = useSelector(store => store.countrys.countrysName);

    const statusName = useSelector(store => store.countrys.statusName);

    const [inputLength, setInputLength] = useState();

    const handleSearch = (e) => {
       const value = e.target.value;
       dispatch(getNameCountrys(value));
       value.length >= 1 ? setInputLength(true) : setInputLength(false)
    }

    return (
        <Router>
            <div className="mt-5">
                <form action="" method="POST" className="form-group">
                    <div className="input-group mb-3">
                        <div className="input-group-prepend">
                            <span className="input-group-text" id="basic-addon1">@</span>
                        </div>
                        <input type="text" onChange={(e) => handleSearch(e)} className="form-control" placeholder="Search for a country..." name="name" aria-describedby="basic-addon1" />
                    </div>
                    {
                        statusName === 'Loading...' ? (
                            <div className="card card-search">
                                <div className="card-body">
                                    <div className="row justify-content-center align-items-center">
                                        <div className="spinner-border text-primary" role="status">
                                            <span className="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ) : ('')
                    }
                    {
                        countrysName.length >= 1 && inputLength === true && statusName !== 'Loading...' ? (
                            <div className="card card-search">
                                <div className="card-body">
                            {
                                countrysName.map(countryName => (
                                    <div className="country-option" key={countryName.name}>
                                        <Link to={`${countryName.name}`}>
                                            <h6 className="box-search">{countryName.name}</h6>
                                        </Link>
                                    </div>
                                ))
                            }
                                </div>
                            </div>
                        ) : ('')
                    }
                    {
                        statusName === 404 ? (
                            <div className="card card-search">
                                <div className="card-body">
                                    <div className="row justify-content-center align-items-center">
                                        <div>
                                            <h3>Error</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ) : ('')
                    }
                </form>
            </div>
        </Router>
    )
}

export default CountrySearch;
