import React from 'react';

//#Styles
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    title: {
        fontWeight: 700,
    },
    darkMode: {
        fontWeight: 500,
    },
    headerBorder: {
        boxShadow: '5px #f5f5f5',
    },
}));

function Header() {
    const classes = useStyles();

    return (
        <div className={classes.headerBorder}>
            <div className="container">
                <div className="row mt-4 mb-4">
                    <div className="col-lg-6">
                        <h4 className={classes.title}>Where in the world?</h4>
                    </div>
                    <div className="row col-lg-6 justify-content-end">
                        <span className={classes.darkMode}>Dark Mode</span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Header;
